import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';

@Injectable()
export class SearchService {
  searchUrl = {
    list: 'https://www.www.www',
    person: 'https://jsonplaceholder.typicode.com/posts',
    mentions: 'https://www.www.www'
  };

  constructor(private http: HttpClient) {}

  getUser(person) {
    return this.http.get(this.searchUrl.person + '?userId=' + person);
  }
}
