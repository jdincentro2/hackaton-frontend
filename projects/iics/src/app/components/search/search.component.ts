import { Component, OnInit } from '@angular/core';
import { routeAnimations } from '../../core/core.module';
import { FormControl, Validators } from '@angular/forms';
import { SearchService } from '../../shared/search.service';

@Component({
  selector: 'iics-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [routeAnimations]
})
export class SearchComponent implements OnInit {
  searchFormControl = new FormControl('', [Validators.required]);

  constructor(private searchService: SearchService) {}

  ngOnInit() {}

  searchQuery(person) {
    this.searchService.getUser(person).subscribe(value => {
      console.log(value);
    });
  }
}
