import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { routeAnimations } from '../../core/core.module';

@Component({
  selector: 'iics-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  animations: [routeAnimations]
})
export class ToolbarComponent implements OnInit {
  stickyHeader$: Observable<boolean>;
  logo = require('../../../assets/logo.png');
  navigation = [
    { link: 'about', label: 'anms.menu.about' },
    { link: 'feature-list', label: 'anms.menu.features' },
    { link: 'examples', label: 'anms.menu.examples' }
  ];
  navigationSideMenu = [
    ...this.navigation,
    { link: 'settings', label: 'anms.menu.settings' }
  ];
  isAuthenticated$: Observable<boolean>;

  constructor() {}

  ngOnInit() {}
}
