import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { Observable } from 'rxjs';

export interface ChartDataType {
  data: number[];
  label: string;
}

export interface ChartData {
  labels: string[];
  data: ChartDataType[];
  legend: boolean;
}

export interface WidgetData {
  span: number;
  id: number;
  type: chartType;
  data: (widget: WidgetData) => Observable<ChartData>;
}

export type chartType = 'line' | 'bar' | 'doughnut' | 'radar' | 'bubble';

@Component({
  selector: 'iics-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WidgetComponent implements OnInit {
  @Input()
  widgetData: WidgetData;
  @Input()
  chartType: chartType;

  @Output()
  resize = new EventEmitter<WidgetData>();

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false,
    aspectRatio: 1
  };

  invalid: boolean;
  labels: string[];
  data: ChartDataType[];
  legend: boolean;

  constructor() {}

  ngOnInit(): void {
    this.invalid = true;
    this.widgetData.data(this.widgetData).subscribe((data: ChartData) => {
      this.invalid = false;
      this.labels = data.labels;
      this.data = data.data;
      this.legend = data.legend;
    });
  }

  grow(data: WidgetData) {
    if (data.span < 9) {
      data.span += 1;
    }
    this.invalidate();
    this.resize.emit(data);
  }

  shrink(data: WidgetData) {
    if (data.span > 2) {
      data.span -= 1;
    }
    this.invalidate();
    this.resize.emit(data);
  }

  invalidate() {
    this.invalid = true;
  }

  reloadChart() {
    this.invalid = false;
  }
}
