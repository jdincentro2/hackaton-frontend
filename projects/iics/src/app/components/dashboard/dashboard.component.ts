import { Component, OnInit } from '@angular/core';
import { WidgetData, ChartData } from '../widget/widget/widget.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'iics-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  widgets: WidgetData[] = [
    { id: 1, span: 2, type: 'doughnut', data: this.getData.bind(this) },
    { id: 2, span: 6, type: 'line', data: this.getData.bind(this) },
    { id: 3, span: 3, type: 'bar', data: this.getData.bind(this) },
    { id: 4, span: 3, type: 'radar', data: this.getData.bind(this) },
    { id: 4, span: 2, type: 'bubble', data: this.getData.bind(this) }
  ];
  ngOnInit() {}

  onResize(data: WidgetData) {}

  getData(widget: WidgetData): Observable<ChartData> {
    return new Observable(observer => {
      const data: ChartData = {
        labels: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
        data: [
          { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
          { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
        ],
        legend: true
      };

      observer.next(data);

      // When the consumer unsubscribes, clean up data ready for next subscription.
      return {
        unsubscribe() {}
      };
    });
  }
}
