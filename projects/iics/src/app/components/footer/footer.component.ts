import { Component, OnInit } from '@angular/core';
import { routeAnimations } from '../../core/core.module';
import { environment as env } from '../../../environments/environment';

@Component({
  selector: 'iics-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  animations: [routeAnimations]
})
export class FooterComponent implements OnInit {
  isProd = env.production;
  envName = env.envName;
  version = env.versions.app;
  year = new Date().getFullYear();
  constructor() {}

  ngOnInit(): void {}
}
